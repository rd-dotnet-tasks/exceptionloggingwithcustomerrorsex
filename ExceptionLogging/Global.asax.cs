﻿using log4net;
using Serilog;
using System;
using System.Text;
using System.Web;
using System.Web.Optimization;
using System.Web.Routing;

namespace ExceptionLogging
{
    public class Global : HttpApplication
    {
        private static readonly ILog _log4net = log4net.LogManager.GetLogger("log4net");
        private static readonly NLog.Logger _nlog = NLog.LogManager.GetCurrentClassLogger();
        private static readonly LoggerConfiguration _serilog = new LoggerConfiguration();

        private void Application_Start(object sender, EventArgs e)
        {
            // Code that runs on application startup
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            log4net.Config.XmlConfigurator.Configure();
        }

        private void Application_Error(object sender, EventArgs e)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("Unhandled error occured in application. Sender: ");
            sb.AppendLine(Request.RawUrl);
            sb.Append("Query: ");
            sb.AppendLine(Request.QueryString.ToString());

            Exception ex = Server.GetLastError().GetBaseException();

            // log4net
            _log4net.Error(sb.ToString(), ex);

            //nlog
            _nlog.Error(sb.ToString());

           var x = _serilog
                .Enrich.WithHttpRequestId()
                .Enrich.WithHttpRequestType()
                .Enrich.WithHttpRequestUrl()
                .Enrich.WithUserName()
                .WriteTo.File("logs\\serilog_log.txt")
                .CreateLogger();
            x.Error(sb.ToString(), ex);
        }
    }
}